 /* 
 * Author:  RAYMONDAUD Quentin
 * helped by : https://medium.com/@xavidramirez/angular-cli-fullstack-nodejs-express-mongoose-part-1-978ea0e3da4d
 */
const errors = require('./components/errors');
const path = require('path');
const session = require('express-session');

// Importing reusable mongoDB service module 
var mongoUtil = require( './components/mongocli/mongoUtils' );
mongoUtil.connectToServer( function( err, client ) {
  if (err) console.log(err);
} );

// Importing reusable PostGreSql service module 
var pgUtil = require( './components/pgutils/pgUtils')
pgUtil.connectToServer( function( err, client ) {
  if (err) console.log(err);
  // start the rest of your app here
} );

// Ranking Service module DB Checker 
//Calls rank per "rankPerScoreMax" each minute ( 60000 ms )
const ranking = require('./components/rankingutils/rankingUtils');
async function rankingUpdater(timer = 30000) {
    while (true) {
        ranking.rankPerScoreMax(function(result){
            if (result){
                serverSocket.broadcast(( JSON.stringify({   type:"RANKING",
                                                            ranking: result.slice(0,10),
                                                            message: "" }))
                )
            }
        });
        await new Promise( resolve => setTimeout(resolve, timer));
    }
}

rankingUpdater(); // Starts the ranking updater promise loop

// FONCTION DE CALCUL DE SCORE PRESENTEE DANS L'ACTIVITE CORRESPONDANTE
// APPEL FAIT LIGNE 378 DANS LE MERGE DES PROMISE DE VERIFICATION DES QUIZZ
function calculScore(dif,nbQuestion,nbBonneRep,tempsMaxParQuestion,tempsUser){
    let timeScore = ( ( (tempsMaxParQuestion/(dif-1)) * nbQuestion) / tempsUser );
    timeScore = ( timeScore >=  dif ? dif : timeScore );
    // console.log( "TimeScore =  ( ( " + (tempsMaxParQuestion/(dif-1))  + " * " + nbQuestion + ") / " + tempsUser + ")");
    // console.log( "TimeScore = " + timeScore);
    return parseInt(( dif * (nbBonneRep/nbQuestion) * timeScore ) * 10000);
}

/**
 * Sets up all the routes for the Express app.
 * For all new endpoints you create you need to add them here
 * @param {Express} app - contains a reference to the Express App in the app.js file in the root of the project
 * @param {__dirname} root -  contains an Object value pointing to the root of the project 
 */
module.exports = function routes(app, root) {

    // return to the client his ip and the protocol used
    app.get('/test', function(req, res){
        // Src: https://stackoverflow.com/questions/23413401/what-does-trust-proxy-actually-do-in-express-js-and-do-i-need-to-use-it
        var ip = req.ip; // trust proxy sets ip to the remote client (not to the ip of the last reverse proxy server)
        if (ip.substr(0,7) == '::ffff:') // fix for if you have both ipv4 and ipv6
            ip = ip.substr(7);
        // req.ip and req.protocol are now set to ip and protocol of the client, not the ip and protocol of the reverse proxy server
        // req.headers['x-forwarded-for'] is not changed
        // req.headers['x-forwarded-for'] contains more than 1 forwarder when
        // there are more forwarders between the client and nodejs.
        // Forwarders can also be spoofed by the client, but 
        // app.set('trust proxy') selects the correct client ip from the list
        // if the nodejs server is called directly, bypassing the trusted proxies,
        // then 'trust proxy' ignores x-forwarded-for headers and
        // sets req.ip to the remote client ip address

        res.json({"ip": ip, "protocol": req.protocol, "headers": req.headers['x-forwarded-for']});
    });

    // 404 Error routing example
    app.route('/api').get(errors[404]);

    // All Other routes get redirected to the index if they not under /api
    app.route('/').get((req, res) => { res.sendFile(path.join(root, 'dist/index.html')); });

    // Destruct MongoDB session when requested to logout
    app.route('/logout').get((req, res) => {
        if( req.session.idPg ){
            pgUtil.setStatus(req.session.idPg, 0, function(result){ // Set offline on pg user table
                if (result){
                    let response = {    id: req.session.idPg,
                                        avatar: req.session.avatar,
                                        nom: req.session.nom,
                                        prenom: req.session.prenom,
                                        date_naissance: req.session.date_naissance,
                                        humeur: req.session.humeur,
                                        statut_connexion: 0 
                                    };
                    // Broadcast to all clients the logout event & infos
                    serverSocket.broadcast( JSON.stringify({    type:"USER",
                                                                userDetail: response,
                                                                message: req.session.prenom + ' ' + req.session.nom + " has Quit !" 
                                                            }))
                    req.session.destroy();
                    res.send({ destruct:true});
                }
            });

        }
        else
            res.send({ destruct:false});
    });

    // POST Resquest handler middleware on '/login' =>
    // Requesting PostGre DBS in order to verify credentials
    // Call PostGreDB using /component/pgutils/pgUtil.js service
    // Requesting MongoDB in order to open sessions if logged successfully
    app.route('/login').post((req,res) => {
        let err, result;
        // If session already exists
        if( req.session.idPg ){
            res.status(200);
            // Build the response : User infos
            response = {    id: req.session.idPg,
                            avatar: req.session.avatar,
                            nom: req.session.nom,
                            prenom: req.session.prenom,
                            date_naissance: req.session.date_naissance,
                            humeur: req.session.humeur,
                            statut_connexion: 1 };
            serverSocket.broadcast( JSON.stringify({   type:"USER",
                                        userDetail: response,
                                        message: req.session.prenom + ' ' + req.session.nom + " is Online !" }));
            setTimeout(() => {
                // If the current user receivee challenge when he was offline
                mongoUtil.getChallengeByUserId(req.session.idPg, function(result2){
                    result2= result2[0];
                    if ( result2 )
                    {
                        broadata =  JSON.stringify({
                                                        type: "DEFI",
                                                        src:result2.id_user_defiant,
                                                        target:result2.id_user_defi,
                                                        idChallenge: result2._id});
                        serverSocket.broadcast( broadata );
                    }
                })
            }, 2000);
            res.send(response);
        }
       
        else{  // If session doesn't already exist
            // PG authentification & info request
            pgUtil.authUser(req.body.username, req.body.password, function(result){
                if ( result ){
                    res.status(200);
                    req.session.isConnected=true;
                    req.session.idPg=req.body.username;
                    req.session.avatar = result.avatar;
                    req.session.nom = result.nom;
                    req.session.prenom = result.prenom;
                    req.session.date_naissance = result.date_naissance;
                    req.session.humeur = result.humeur;
                    req.session.statut_connexion = 1;
                    req.session.idPg = result.id;
                    // Build the response : User infos
                    response = {    id: req.session.idPg,
                                    avatar: req.session.avatar,
                                    nom: req.session.nom,
                                    prenom: req.session.prenom,
                                    date_naissance: req.session.date_naissance,
                                    humeur: req.session.humeur,
                                    statut_connexion: 1 };
                    serverSocket.broadcast( JSON.stringify({   type:"USER",
                                                                userDetail: response,
                                                                message: req.session.prenom + ' ' + req.session.nom + " is Online !" }));
                    setTimeout(() => {
                        // If the current user receivee challenge when he was offline
                        mongoUtil.getChallengeByUserId(req.session.idPg, function(result2){
                            result2= result2[0];
                            if ( result2 )
                            {
                                broadata =  JSON.stringify({
                                                                type: "DEFI",
                                                                src:result2.id_user_defiant,
                                                                target:result2.id_user_defi,
                                                                idChallenge: result2._id
                                                            });
                                serverSocket.broadcast( broadata );
                            }
                        })
                    }, 2000);
                    res.send(response);
                }
                else{        
                    //console.log("Auth Denied");
                    res.status(400);
                    res.send(false)
                }
            })
        }
    });
    
    // Update pgSql with req.bodu.bio & req.body.imgUrl passed through Port request on /updateProfile
    // Call PostGreDB using /component/pgutils/pgUtil.js service
    app.route('/updateprofile').post((req,res) => {
        let err, result;
        // If session  exists
        if( req.session.idPg ){
            let values=[];
            let fields=[];
            if ( req.body.imgUrl != req.session.avatar){
                values.push(req.body.imgUrl);
                fields.push("avatar");
            }
            if ( req.body.bio != req.session.humeur){
                values.push(req.body.bio);
                fields.push("humeur");
            }
            // Update profile
            pgUtil.updateFields(fields, values, req.session.idPg, function(result){
               if ( result.rowCount > 0){ // If succeed
                    req.session.avatar = req.body.imgUrl;
                    req.session.humeur = req.body.bio;
                    response = {    id: req.session.idPg,
                                    avatar: req.session.avatar,
                                    nom: req.session.nom,
                                    prenom: req.session.prenom,
                                    date_naissance: req.session.date_naissance,
                                    humeur: req.session.humeur,
                                    statut_connexion: req.session.statut_connexion };
                    res.status(200);
                    res.send(response);
               }
               else{ // If failed
                    //console.log("Profile not updated")
                    res.status(200); // Wrong status but ok for now
                    res.redirect(307,'/');
               }
            })
        } else { 
            //console.log("Access Denied");
            res.status(400);
            res.send(false)
        }
    });

    // Get user infos by ID
    // Call PostGreDB using /component/pgutils/pgUtil.js service
    app.route('/user/:id').get((req, res) => {
        if( req.session.idPg ){
            // console.log(req)
            pgUtil.getUserById(req.params.id,function(result){
                if ( result ){ // If succeed
                    response = {    id: result.id,
                                    avatar: result.avatar,
                                    nom: result.nom,
                                    prenom: result.prenom,
                                    date_naissance: result.date_naissance,
                                    humeur: result.humeur,
                                    statut_connexion: result.statut_connexion };
                    res.status(200);
                    res.send(response);
               }
            });
        }
        else{
            res.status(400);
            res.send(false);
        }
    })

    // Get user satistics by ID
    // Call MongoDb using /component/mongoutils/mongoUtil.js service  
    app.route('/user/:id/playerstat').get((req, res) => {
        if( req.session.idPg ){
            mongoUtil.getPlayerStat( req.params.id, function(result){
                if (result) {
                    res.status(200);
                    res.send(result[0]);
                }
                else{
                    res.status(400);
                    res.send(false);
                }
            });
        }
        else{
            res.status(400);
            res.send(false);
        }
    })

    // Get users history elements from [startIndex] to [startIndex + nb] included
	// Call MongoDb using /component/mongoutils/mongoUtil.js service 
    app.route('/user/:id/playerhistory/:startIndex/:nb').get((req, res) => {
        if( req.session.idPg ){
            mongoUtil.getHistory( req.params.id, req.params.startIndex, req.params.nb , function(result){
                if (result) {
                    res.status(200);
                    res.send(result);
                }
                else{
                    res.status(400);
                    res.send(false);
                }
            });
        }
        else{
            res.status(400);
            res.send(false);
        }
    })

    // Get challenge history of a user from startIndex to startIndex + nb
    app.route('/user/:id/challengehistory/:startIndex/:nb').get((req, res) => {
        if( req.session.idPg ){
            pgUtil.getChallengeHistory( req.params.id, req.params.startIndex, req.params.nb , function(result){
                if (result) {
                    res.status(200);
                    res.send(result);
                }
                else{
                    res.status(400);
                    res.send(false);
                }
            });
        }
        else{
            res.status(400);
            res.send(false);
        }
    })

    // Get pg users history elements from [startIndex] to [startIndex + nb] included
    // Call MongoDB using /component/pgutils/pgUtil.js service 
    app.route('/user/:id/pgplayerhistory/:startIndex/:nb').get((req, res) => {
        if( req.session.idPg ){
            pgUtil.getHistory( req.params.id, req.params.startIndex, req.params.nb , function(result){
                if (result) {
                    res.status(200);
                    res.send(result);
                }
                else{
                    res.status(400);
                    res.send(false);
                }
            });
        }
        else{
            res.status(400);
            res.send(false);
        }
    })


    // Get users from [startIndex] to [startIndex + nb ] included
    // Call PostGreDB using /component/pgutils/pgUtil.js service
    app.route('/user/list/:startIndex/:nb').get((req, res) => {
        if( req.session.idPg ){
            // console.log(req)
            pgUtil.getUsers(parseInt(req.params.startIndex), parseInt(req.params.nb),function(result){
                if ( result ){ // If succeed
                    // console.log(result);
                    res.status(200);
                    res.send(result.rows);
               }
            });

        }
        else{
            res.status(400);
            res.send(false);
        }
    })

    // Get Quizz Theme List
	// Call MongoDb using /component/mongoutils/mongoUtil.js service 
    app.route('/themes').get((req, res )=>{
        if( req.session.idPg ){
            
            mongoUtil.getThemes(function(result){
                if (result) {
                    res.status(200);
                    res.send(result);
                }
                else{
                    res.status(400);
                    res.send(false);
                }
            });
        }
        else{
            res.status(400);
            res.send(false);
        }
    })

    // Get ranking data from server
    app.route('/ranking').get((req,res )=>{
        if ( req.session.idPg){
            res.status(200);
            res.send(ranking.getRanking())
        }
        else {
            res.status(400);
            res.send(false);
        }
    })
    
    // Get a Quizz built by theme and difficulty
	// Call MongoDb using /component/mongoutils/mongoUtil.js service 
    app.route('/quizz/:theme/:difficulty').get((req, res )=>{
        if( req.session.idPg ){
            // console.log(req.params.theme+"/"+req.params.difficulty);
            mongoUtil.getQuizzByThemeAndDifficulty(req.params.theme, req.params.difficulty,function(result){
                if (result){
                    res.status(200);
                    res.json(result);
                }
                else{
                    res.status(400);
                    res.send(false);
                }
            });
        }
        else{
            res.status(400);
            res.send(false);
        }
    })

    // Challenge user with a quizz the user just finished
    // Insert into mongodb defi document
    app.route('/quizz/challenge').post((req,res) => {
        if( req.session.idPg ){
            mongoUtil.addChallenge(req.body.quizz, req.session.idPg, req.body.targetUserId, req.body.score, function(result){
                if ( result != undefined &&  result.insertedId ){
                    let idChallenge = result.ops[0]._id;            
                    mongoUtil.assignChallenge(req.session.idPg, result.insertedId, function(result2){
                        if ( result2 != undefined ){
                            //console.log(" Challenge de "+ req.session.idPg + " sur " + req.body.targetUserId );
                            // Broadcast defi info to all clients
                            serverSocket.broadcast( JSON.stringify({
                                                                    type: "DEFI",
                                                                    src:req.session.idPg,
                                                                    target:req.body.targetUserId,
                                                                    idChallenge: idChallenge,  
                                                                    message: req.session.nom + " " + req.session.prenom + " challenged someone !"
                                                                }));
                            res.status(200);
                            res.send(true);
                        }
                    })
                }
                else{
                    //console.log("Defi Louche");
                    res.status(400);
                    res.send(false);
                }
                
            });
        } else {
            res.status(400);
            res.send(false);  
        }
        
    })

    // Get answer if a user accepts the challenge or not
    // Refusing a challenge is considered as losing it
    // Else he gets the quizz 
    app.route('/quizz/challenge/:quizzId/:accepted').get((req,res) => {
        if ( req.session.idPg){
            //console.log("Challenge Denied");
            mongoUtil.getChallenge(req.params.quizzId, function(result){
                if ( result[0] ){
                    challengeRes = result;
                    // If the user accepts the challenge
                    if ( req.params.accepted != "false"){
                        res.status(200);
                        res.send(challengeRes);
                        return;
                    }
                    let idWinner=challengeRes[0].id_user_defiant
                    let idLooser=challengeRes[0].id_user_defi
                    // If the user refuses the challenge :Inserts a challenge history into pg table
                    pgUtil.insertDefiHistory(idWinner,idLooser, function(result1){
                        if ( result1 ){                        

                            if( result1  && ( idWinner == req.session.idPg || idLooser == req.session.idPg ) ){
                                // Removes the challenge from mongodb document
                                mongoUtil.removeChallenge(req.params.quizzId, function(result2){
                                    if (result2){
                                        if ( req.params.accepted == "false"){
                                            res.status(200);
                                            res.send(false);
                                        }
                                        setTimeout(() => { // Send the next challenge after 2 sec if it exists
                                            mongoUtil.getChallengeByUserId(req.session.idPg, function(result2){
                                                result2= result2[0];
                                                if ( result2 )
                                                {
                                                    broadata =  JSON.stringify({
                                                                                    type: "DEFI",
                                                                                    src:result2.id_user_defiant,
                                                                                    target:result2.id_user_defi,
                                                                                    idChallenge: result2._id,  
                                                                                    message: req.session.prenom + " challenged someone !"});
                                                    serverSocket.broadcast( broadata );
                                                }
                                            })
                                        }, 2000);
                                    }
                                })
                            }
                        }
                    })
                } else {
                    res.status(400);
                    res.send(false); 
                }
            })
        } else {
            res.status(400);
            res.send(false);  
        }

    })

    // Verify quizz responses, fills classtok and histok tables with trusted data about user perfs
	// Call MongoDb using /component/mongoutils/mongoUtil.js service 
    // The tricky route !
    // Cette route délégue beaucoup de travail au serveur MongoDB
    // Je pense que cela permettra de tenir une charge supérieure en évitant au serveur Node de faire les vérification
    // Ici chaque question est vérifié indépendement par un Promise effectuant une requete MONGODB
    // 1 requete par vérification = 1 promise par vérification
    // Cela permet aussi de pouvoir potentiellement faire des quizz multi theme
    app.route('/quizz/verify').post((req,res) => {
        // If session  exists and it's the client's one
        if( req.session.idPg == req.body.idPostGre ){
            let nbQuestion = nbGoodAnswer = 0;
            let correction = [];
            let promiseArray = [];
            // Pour chaqué réponse fait une requete mongo
            for ( let response in req.body.responses ){
                // Quizz MonoThème donc hors de la boucle 
                let resp = req.body.responses[response].response;
                // Creates promises ( async ) for each quizz answer to verify
                promiseArray.push( new Promise((resolve,reject) => { 
                    mongoUtil.getResponseByIdAndTheme(  req.body.responses[response].theme,
                                                        req.body.responses[response].idQuestion, 
                                                        function(result){
                                                            if ( result[0] && result[0].quizz[0]['réponse'] ){
                                                                nbQuestion += 1;
                                                                let isCorrect =  result[0].quizz[0]['réponse'].trim() == resp.trim();
                                                                if ( isCorrect ){
                                                                    nbGoodAnswer += 1;
                                                                    correction.push({   anecdote: result[0].quizz[0]['anecdote'],
                                                                                        question: result[0].quizz[0]['question'],
                                                                                        reponse: resp});
                                                                }
                                                                else
                                                                    correction.push({   anecdote: result[0].quizz[0]['anecdote'],
                                                                                        question: result[0].quizz[0]['question'],
                                                                                        reponse: resp,
                                                                                        correction: result[0].quizz[0]['réponse']});
                                                            }
                                                            resolve();
                                                        });
                                                     }));
            }
            
            // Remplissage des tables avec le résultat de toutes les requetes lancées les promises
            merge =  async function(promiseArray) {
                await Promise.all(promiseArray);

                let idWinner = idLooser = srcScore = undefined;
                // Un simple calcul pour affecter les valeurs au joueur.
                let score = calculScore(parseInt(req.body.difficulty), nbQuestion, nbGoodAnswer, 6, parseFloat(req.body.elapsedTime));

                // If this result is a challenge 
                if ( req.body.challengeId){ // If this quizz answer is a challenge answer
                    mongoUtil.getChallenge(req.body.challengeId, function(result){ // Get the answer from db
                        if ( result[0] ){
                            result = result[0];
                            srcScore =  result.score_user_defiant;
                            if ( result.score_user_defiant > score ){
                                idWinner=result.id_user_defiant
                                idLooser=result.id_user_defi
                            } else {
                                idLooser=result.id_user_defiant;
                                idWinner=result.id_user_defi;
                            }
                            // Inserts challenge history into hist_defi PG table 
                            pgUtil.insertDefiHistory(idWinner,idLooser, function(result1){
                                // console.log(result1)
                                if( result){
                                    // Removes the challenge into mongodb defi document
                                    mongoUtil.removeChallenge(req.body.challengeId, function(result2){
                                        // console.log(result2)
                                    })
                                    // When a challenge is completed the next is sent to the player
                                    setTimeout(() => {
                                        mongoUtil.getChallengeByUserId(req.session.idPg, function(result2){
                                            result2= result2[0];
                                            if ( result2 )
                                            {
                                                broadata =  JSON.stringify({
                                                                            type: "DEFI",
                                                                            src:result2.id_user_defiant,
                                                                            target:result2.id_user_defi,
                                                                            idChallenge: result2._id,  
                                                                            message: req.sessions.nom + " " + req.session.prenom + " challenged someone !"});
                                                serverSocket.broadcast( broadata );
                                            }
                                        })
                                    }, 2000);
                                }
                            })
                        }
                    })
                }
                // History data fill 
                const newHisto = {  idPostGre:req.session.idPg,
                                    date: new Date(),
                                    difficulte: req.body.difficulty,
                                    temps:req.body.elapsedTime,
                                    nbQuestion: nbQuestion,
                                    nbBonneReponse: nbGoodAnswer,
                                    score: score,
                                    historique: correction
                            };
                // Answer to the client requestion construction
                const cliResults = {    temps:req.body.elapsedTime,
                                        nbQuestion: nbQuestion,
                                        nbBonneReponse: nbGoodAnswer,
                                        score: score,
                                        difficulte: req.body.difficulty,
                                        correction: correction
                                    };
                // Inserts result into my personnal mongodb document 
                mongoUtil.insertQuizzResult(newHisto, function(result){
                    if (result){
                        // Insert newHisto into my personnal mongodb histok document
                        mongoUtil.updatePlayerRank(newHisto, function(result){
                            if (result){
                                res.status(200);
                                if ( req.body.challengeId ){
                                    cliResults.idWinner = idWinner;
                                    cliResults.srcScore = srcScore;
                                }
                                // Sends cliResults as response
                                res.json(cliResults);
                            }
                            else{
                                res.status(400);
                                res.send(false);
                            }
                        });   
                    }
                    else{
                        res.status(400);
                        res.send(false);
                    }
                });
                try{
                    // Inserts challenge history in pg table
                    pgUtil.insertUserHistory(req.session.idPg, req.body.difficulty, 
                                         nbGoodAnswer, parseInt(req.body.elapsedTime), score, function(result){
                        if ( result ){ 
                            // If succeed 
                        }
                    });
                }
                catch(err){ console.log(err) }
            };
            try{ // Merge the promise array
                merge(promiseArray);
            }
            catch(err){ console.log(err) }
           
        } else {
            res.status(400);
            res.send(false)
        }
    });

};
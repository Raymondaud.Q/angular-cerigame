// RAYMONDAUD Quentin

var _quizz =new Object();

// returns an integer between 0 and max
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}



// returns x propositions excluding the response
function getXProps(response,x,props){
	while ( props.length-1 > x ){
		let rndIndex = getRandomInt(props.length-1);
		if (  props[rndIndex] !=  response ){
			props.splice(rndIndex,1);
		}
	}
	//console.log(props)
	return props;
}


// Exportable quizz builder module
module.exports = {


	// Construct  a quizz with 5 question in a singular theme
	// We can modidy the question number by providing quizzLength argument 
	themedQuestions: function(fullQuizz,difficulty, quizzLength = 5){

		_quizz['theme'] = fullQuizz[0]['thème'];
		_quizz['_id'] = fullQuizz[0]['_id'];
		_quizz['redacteur'] = fullQuizz[0]['rédacteur'];
		_quizz['fournisseur'] = fullQuizz[0]['fournisseur'];

		let index = [];
		_quizz['quizz'] = [];
		for ( let i = 0 ; index.length < quizzLength ; i ++){

			let rndIndex = getRandomInt(fullQuizz[0]['quizz'].length);
			if ( ! index.includes(rndIndex)){

				index.push(rndIndex);
				let props = getXProps(fullQuizz[0]['quizz'][rndIndex]['réponse'], difficulty , fullQuizz[0]['quizz'][rndIndex]['propositions'] )
				_quizz['quizz'].push( { propositions: props,
										question:fullQuizz[0]['quizz'][rndIndex]['question'], 
										id:fullQuizz[0]['quizz'][rndIndex]['id'] 
										}
									);
			}
		}
		return _quizz;
	}
};
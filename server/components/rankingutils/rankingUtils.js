// RAYMONDAUD Quentin

// Importing reusable PostGreSql service module 
var pgUtil = require( '../pgutils/pgUtils')
pgUtil.connectToServer( function( err, client ) {
  if (err) console.log(err);
  // start the rest of your app here
} );

// Comparator used in ranking algos
function rankSort(a,b){
	if ( a.score > b.score ){
		return -1;
	}
	if ( a.score < b.score ){
		return 1;
	}
	return 0;
}
var _ranking =[];

// Exportable ranking processing algorithms
module.exports = {



	// Cross the pg history table and construct the ranking
	// Rank by max score
	// pgUtil call
	/* returns by callback Array of 
		id_user,
		score */
	rankPerScoreMax: function(callback){
		pgUtil.getMostRecentHistory(function(result){
			_ranking = [];
			let scoreMax = [];
			let ids = []
			console.log(result.length)
			for ( let res of result ){
				let idUser = res.id_user;
				if ( ! ids.includes(idUser)){
					ids.push(idUser);
					scoreMax[idUser] = res.score;
				} else {
					let sm = scoreMax[idUser];
					scoreMax[idUser] = ( res.score > sm ? res.score : sm );
				}
			}
			for ( let index of ids ){
				_ranking[index]=({ 	idUser: index, 
									score: scoreMax[index],
								})
			}
			_ranking.sort(rankSort);
			return callback(_ranking);
		})
		return callback(undefined);
	},
	
	// NOT USED
	// Cross the pg history table and construct the ranking
	// Rank by max (mean score)
	// pgUtil call
	/* returns by callback Array of 
		id_user:
		score */
	rankPerMeanScore: function(callback){
		pgUtil.getMostRecentHistory(function(result){
			_ranking=[]
			let nbGame = [];
			let sumScore = [];
			let ids = []
			for ( let res of result ){
				let idUser = res.id_user;
				if ( ! ids.includes(idUser)){
					ids.push(idUser);
					sumScore[idUser] = 0;
					nbGame[idUser] = 0;
				}
				sumScore[idUser] += res.score;
				nbGame[idUser] +=1;
			}
			for ( let index of ids ){
				_ranking[index]=({ 	
									idUser: index, 
									score: parseInt(sumScore[index]/nbGame[index]),
									nbGame: nbGame[index]
								})
			}
			_ranking.sort(rankSort);
			return callback(_ranking);
		})
		return callback(undefined);
		
	},

	// Returns a slice of the ranking array ( Per Score Max )
	getRanking:function(startIndex=0, nb=10){
		return _ranking.slice(startIndex, startIndex+nb);
	}


}





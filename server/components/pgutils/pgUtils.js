/* 
 * Author:  RAYMONDAUD Quentin
 * helped by : https://stackoverflow.com/questions/24621940/how-to-properly-reuse-connection-to-mongodb-across-nodejs-application-and-module
 */

const config = require('../../env/env.json')[process.env.NODE_ENV || 'dev'];

// Postgre service module setup
const { Pool } = require('pg');
const { Client } = require('pg');

var _pgPool;
var _pgClient;


// Exportable PG BackEnd Service module
module.exports = {

    // Connect to PostGre server and construct a PgDriver Pool & Client
    connectToServer: function( callback ) {
        _pgPool = new Pool({
            user: 'uapv1702675',
            // Look at ./env/env.json
            host: config.PG_HOST, // If NODE_ENV=prod  
                                    // config.PG_HOST = 127.0.0.1
                                // else 
                                    // config.PG_HOST = pedago.univ-avignon.fr
            database: 'etd',
            password: 'BNG8Lb',  // Possible to remove and provide with node args
            port: 5432,
        });

        _pgClient = new Client({
            user: 'uapv1702675',
            host: config.PG_HOST,
            database: 'etd',
            password: 'BNG8Lb',  // Possible to remove and provide with node args
            port: 5432,
        });
    },

    // Returns the driver Pool instance
    getPool: function() {
        return _pgPool;
    },

    // Returns the driver Client instance
    getClient: function() {
        return _pgClient;
    },

    // Update user connexion status
    setStatus: function(idPg, param, callback){
        return _pgPool.query("update fredouil.users set statut_connexion=$1 where id=$2", [param, idPg],
            (err, result) => { 
                if ( err) 
                    return callback(undefined)
                else
                    return callback(true);
            })
    },

    // Authentification PostGre
    authUser: function(username, password, callback){
        return _pgPool.query("select id,motpasse,nom,prenom,date_naissance,avatar,humeur,statut_connexion "+
                                " from fredouil.users where identifiant=$1", [username],
        (err, result) => { 
            if ( result )
                if ( result.rows.length < 1 )
                    return callback(undefined);

                else if (password == result.rows[0].motpasse ){
                    this.setStatus(result.rows[0].id,1, function(result){
                        
                    });
                    let response = {
                        avatar: result.rows[0].avatar,
                        nom: result.rows[0].nom,
                        prenom: result.rows[0].prenom,
                        date_naissance: result.rows[0].date_naissance,
                        humeur: result.rows[0].humeur,
                        statut_connexion: result.rows[0].statut_connexion,
                        id: result.rows[0].id
                    };
                    return callback(response);
                }
                else    
                    return callback(undefined); // Wrong  credentials
            return callback(undefined) //  No PG response
        })
    },

    // Update user profile 'humeur' & 'avatar'
    // Beaucoup plus vulnérable aux injections SQL que les autres routes
    // A faire en deux fonctions pour eviter SQL injections
    updateFields:function(fields, values, idPg, callback){
        let strQuery ="update fredouil.users set "
        let nbField = fields.length;
        for ( let i = 0; i < nbField; i ++){
            strQuery += fields[i] + "='" + values[i]+"'";
            if ( i  < nbField-1)
                strQuery+= ", "
        }
        strQuery += " where id=$1";
        //console.log( strQuery )
        return _pgPool.query(strQuery,[idPg],
        (err, result) => { 
            if (err)
               return callback(err);
            return callback(result); 
        })
    },

    // Get user details by id
    getUserById: function(id, callback){
        return _pgPool.query("select id,motpasse,nom,prenom,date_naissance,avatar,humeur,statut_connexion "+
                                " from fredouil.users where id=$1", [id],
        (err, result) => { 
            if ( result ){
                //console.log(result);
                if ( result.rows.length < 1 )
                    return callback(undefined);

                else {
                    let response = {
                        avatar: result.rows[0].avatar,
                        nom: result.rows[0].nom,
                        prenom: result.rows[0].prenom,
                        date_naissance: result.rows[0].date_naissance,
                        humeur: result.rows[0].humeur,
                        statut_connexion: result.rows[0].statut_connexion,
                        id: result.rows[0].id
                    };
                    return callback(response);
                }
            }
            return callback(undefined) //  No PG response
        })
    },

    // Get a part of user list ( from startIndex to startIndex+nb)
    getUsers: function(startIndex, nb, callback){
        let endIndex = (startIndex + nb);
        //console.log(endIndex);
        return _pgPool.query("select id,nom,prenom,date_naissance,avatar,humeur,statut_connexion "+
                                " from fredouil.users where id between $1 and $2", [startIndex, endIndex], 
        (err, result) => { 
            if ( result ){
                //console.log(result);
                if ( result.rows.length < 1 )
                    return callback(undefined);

                else {
                    return callback(result);
                }
            }
            return callback(undefined) //  No PG response
        })
    },

    // Get postgre history from history[startIndex] to history[startIndex+nb] from 'historique' table
    getHistory( id, startIndex, nb, callback){
        let strQuery = "SELECT id_user, date_jeu, niveau_jeu, nb_reponses_corr, temps, score "+
        "from fredouil.historique WHERE id_user = $1 ORDER BY date_jeu DESC OFFSET $2 LIMIT $3";
        
        return _pgPool.query(strQuery, [id, startIndex, nb],
        (err, result) => { 
            if ( result ){
                if ( result.rows.length < 1 )
                    return callback(undefined);

                else {
                    let pgResult = [];
                    for ( let row of result.rows ){
                        pgResult.push({
                            date_jeu : row.date_jeu,
                            niveau_jeu : row.niveau_jeu,
                            nb_reponses_corr: row.nb_reponses_corr,
                            temps : row.temps,
                            score : row.score
                        })
                    }
                    return callback(pgResult);
                }
            }
            return callback(undefined) //  No PG response
        })
    },

    // Get postgre challenge history from 'hist_defi' table
    getChallengeHistory( id, startIndex, nb, callback){
        let strQuery = "SELECT id_user_gagnant, id_user_perdant, date_defi "+
        "from fredouil.hist_defi WHERE id_user_gagnant = $1 ORDER BY date_defi DESC OFFSET $2 LIMIT $3";
        return _pgPool.query(strQuery, [id, startIndex, nb],
        (err, result) => { 
            if ( result ){
                if ( result.rows.length < 1 )
                    return callback(undefined);

                else {
                    let pgResult = [];
                    for ( let row of result.rows ){
                        pgResult.push({
                            date : row.date_defi,
                            looser : row.id_user_perdant,
                            winner : row.id_user_gagnat
                        })
                    }
                    return callback(pgResult);
                }
            }
            console.log(err);
            return callback(undefined) //  No PG response
        })
    },

    // Returns the id_user & score of the last 500 ( by default) fredouil.historique record
    getMostRecentHistory(callback, nbMostRecent=500){
        return _pgPool.query('SELECT id_user, score from fredouil.historique ORDER BY id desc LIMIT $1;', [nbMostRecent],
            (err, result)=> {
                if (result){
                    if (result.rows.length < 1)
                        return callback(undefined);
                    else{
                        let pgResult = [];
                        for ( let row of result.rows ){
                            pgResult.push(row)
                        }
                        return callback(pgResult);
                    }
                }
                else
                    console.log(err)
                return callback(undefined);
            })
    },

    // Inserts quizz result into 'historique' table
    insertUserHistory(idUser, gameLvl, nbCorrectAnswer, time, score, callback){
        return _pgPool.query("INSERT INTO fredouil.historique (id_user, date_jeu, niveau_jeu, nb_reponses_corr, temps, score)"+
                                "VALUES ($1, NOW() , $2, $3, $4, $5)", [idUser, gameLvl, nbCorrectAnswer, time, score],
        (err, result) => { 
            if ( result )
                return callback(result);
            if ( err)
                console.log(err);
            return callback(undefined) //  No PG response
        })
    },

    // Inserts a challenge history into 'hist_defi' table
    insertDefiHistory(idWinner, idLooser, callback){
        return _pgPool.query("INSERT INTO fredouil.hist_defi (id_user_gagnant, id_user_perdant, date_defi )"+
                                "VALUES ($1, $2, NOW() ) RETURNING * ", [idWinner, idLooser],
        (err, result) => { 
            if ( result )
                return callback(result);
            if ( err)
                console.log(err);
            return callback(undefined) //  No PG response
        })
    },

    // Useless
    updateDefiHistory(idWinner, idLooser, id, callback){
        return _pgPool.query("UPDATE fredouil.hist_defi  SET id_user_gagnant=$1, id_user_perdant=$2, date_defi=NOW() WHERE id=$3 "
            , [idWinner, idLooser, id],
        (err, result) => { 
            if ( result )
                return callback(result);
            if ( err)
                console.log(err);
            return callback(undefined) //  No PG response
        })
    }
};
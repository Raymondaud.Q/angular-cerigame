/* 
 * Author:  RAYMONDAUD Quentin
 * helped by : https://stackoverflow.com/questions/24621940/how-to-properly-reuse-connection-to-mongodb-across-nodejs-application-and-module
 */

const MongoClient = require( 'mongodb' ).MongoClient;
var ObjectId = require('mongodb').ObjectID;
var quizzUtil = require( '../quizzutils/quizzUtils')
// When Prod mode the used mongo is on pedago
// When dev mode the used mongo is a copy on localhost
// Make sure you have a mongo with quizz collection
const url = "mongodb://127.0.0.1:27017";
var _db;


// Exportable MongoDb BackEnd service module 
module.exports = {

    // Connects MongoDB server and construct a MongoDriver Client
    connectToServer: function( callback ) {
        MongoClient.connect( url,  { useUnifiedTopology: true,
                                     useNewUrlParser: true },
                                     function( err, client ) {
                                        _db  = client.db('db');
                                        return callback( err );
                                    } );
    },

    // Returns the current MongoClient
    getDb: function() {
        return _db;
    },

    // Lists all themes of the quizz mongodb document
    getThemes: function(callback){
        return _db.collection("quizz")
                    .distinct('thème',(err, result) => {
                            if (err)
                                return callback(undefined);
                            return callback(result);
                    });

    },

    // Built a quizz with a specific theme & difficulty
    getQuizzByThemeAndDifficulty: function(theme, difficulty, callback){
        console.log('Create a quizz')
        return _db.collection("quizz")
                    .find({ thème: theme } ).toArray( function(err, result) {
                        if (err) return callback(undefined);

                        // If no arg provided default quizz length is 5
                        // quizz = quizzUtil.themedQuestions(result, difficulty);

                        // We can provide quizzLength :
                        quizz = quizzUtil.themedQuestions(result, difficulty, 5 + Math.floor(Math.random() * Math.floor(5)));

                        return callback(quizz);
                    });
    },

    // This one is trickyyyyyyyy
    // In console : 
    // mongo > use db > db.quizz.find( {thème:"Linux"}, {quizz:{$elemMatch:{id:1}},'quizz.réponse':1,'_id':0, 'quizz.anecdote':1} )
    // Thanks to https://stackoverflow.com/questions/52997717/mongodb-and-nodejs-with-express/52998931 for indicate .project() function
    getResponseByIdAndTheme: async function(theme, idQ, callback){
        console.log("Verify a response")
        return  _db.collection("quizz")
                    .find( {thème:theme}).project({quizz:{$elemMatch:{id:idQ}}, "quizz.anecdote":1, "quizz.question":1, "quizz.réponse":1, "_id":0, }).toArray( function(err, result) {
                        if (err) return callback(undefined);
                        return callback(result);
                    });
    },

    // Gets my personnal history system data from histok document
    getHistory( id, startIndex, nb, callback){
        return  _db.collection("histok")
                        .find( {idPostGre: parseInt(id)})
                            .sort({date:-1})
                                .toArray( function(err, result) {
                                    if (err) return callback(undefined);
                                    return callback(result.slice(startIndex,startIndex+nb));
                                });

   },


   // Gets stats from my stats system in clastok document
   getPlayerStat( id, callback){
        return  _db.collection("clastok")
                        .find( {idPostGre: parseInt(id)})
                            .toArray( function(err, result) {
                                if (err) return callback(undefined);
                                return callback(result);
                            });

   },

    // Inserts hsitory into my system in histok document
    insertQuizzResult: function(quizzResult, callback){
        return _db.collection("histok")
                    .insertOne(quizzResult, function(err, result) {
                        if (err) return callback(undefined)
                        console.log("1 result added into db.'histok'");
                        return callback(result);
                    });
    },

    // Updates player global stats in clastok document
    updatePlayerRank: function(quizzResult,callback){
        var myquery = { idPostGre: quizzResult.idPostGre };
        var newvalues = {   $inc:{  
                                nbTotalReponse: quizzResult.nbQuestion,
                                nbBonneReponse: quizzResult.nbBonneReponse,
                                nbQuizz: 1,
                                sommeScore: quizzResult.score,
                                tempsTotal: quizzResult.temps
                            },
                            $set:{
                                idPostGre: quizzResult.idPostGre
                            }   };

        return _db.collection("clastok")
                    .updateOne(myquery, newvalues,  { upsert: true }, function(err, result) {
                        if (err) return callback(undefined);
                        console.log("1 playerRank updated into db.'clastok'");
                        return callback(result)
                    });
    },


    // Adds a challenge in clastok document
    assignChallenge: function( idSource, idDefi, callback ){

        var myquery = { idPostGre: idSource };
        var newvalues = {   $push:{  
                                idDefi: idDefi
                            }
                        };

        return _db.collection("clastok")
                    .updateOne(myquery, newvalues,  { upsert: true }, function(err, result) {
                        if (err) return callback(undefined);
                        console.log("1 Challenge ID assigned to user into db.'clastok' ");
                        return callback(result)
                    });
    },


    // Gets a specific challenge by ID in defi document
    getChallenge: function( idChallenge, callback ){
        return _db.collection("defi")
                    .find({_id:ObjectId(idChallenge)})
                        .toArray( function(err, result) {
                                if (err || result == undefined ) return callback(undefined);
                                console.log("1 Challenge get from db.'defi'");
                                return callback(result);
                            });
    },


    // Returns the challenge array assigned to a user in defi document
    getChallengeByUserId( idUser, callback ){
        return _db.collection("defi")
                    .find( { id_user_defi:idUser } )
                        .toArray( function(err, result) {
                            if (err || result == undefined){
                                console.log(err);
                                return callback(undefined);
                            }
                            return callback(result);
                        })
    },

    // Adds a challenge in defi document
    addChallenge: function( quizz, idSource, idTarget, score, callback ){
        var newvalues = {
                            id_user_defiant : idSource,
                            id_user_defi : idTarget,
                            score_user_defiant : score,
                            quizz: quizz 
                        };

        return _db.collection("defi")
                    .insertOne( newvalues, function(err, result) {
                        if (err) return callback(undefined);
                        console.log(" 1 Challenge added into db.'defi'");
                        return callback(result);
                    });
    },

    // Removes a challenge by ID in defi document
    removeChallenge: function( idChallenge, callback){
        return _db.collection("defi")
                    .deleteOne({_id:ObjectId(idChallenge)}, function(err, result) {
                                if (err) return callback(undefined);
                                console.log("1 Challenge remove from db.'defi'");
                                return callback(result);
                            });
    }
};

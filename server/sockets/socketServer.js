const WebSocket = require('ws'); // new

// ServerSocket module broadcast function
function broadcast(server,data) {
    server.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN){
            client.send(data);
        }
    });
}

// Exportable ServerSocket module
module.exports = function serverSocket(server){

	const serverSocket = new WebSocket.Server({server:server});

	serverSocket.broadcast = function(data){
		//console.log( " ENVOIE DE  => " + data)
		broadcast(serverSocket,data);
	}
	
	// On connection event triggered :
	serverSocket.on('connection', (socketClient) => {
	    //console.log('connected');
	    //console.log('client Set length: ', serverSocket.clients.size);

	    // When a connected client sends a message to server
	    socketClient.on('message', function incoming(message) {
	    	// No processings needed on socketclient push, we have http server designed for that
	    	// It just logs messages
	    	console.log(message);
		});

	    // On close connexion event :
	    socketClient.on('close', (socketClient) => {
	        //console.log('closed');
	        //console.log('Number of clients: ', serverSocket.clients.size);
	    });

	});
	return serverSocket
}


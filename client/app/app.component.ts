/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import * as sha1 from 'js-sha1';
import { User } from './interfaces/user';
import { UserService } from './httpservices/user.service';
import { QuizzService } from './httpservices/quizz.service';
import { SocketService } from './httpservices/socket.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// Main component 
// Holds the navvbar and inner views
export class AppComponent implements OnInit {
    form: FormGroup;
    userData: User; // 
    authFailed: Boolean = false;
    title = 'Ceri-Game';
    notifMsg = '';
    popColor = {'background-color' : 'blue'};

    // Subscribe to the handmade User observable
    ngOnInit(): void { 
        this.userService.user.subscribe(
            usr => {
                this.userData = usr;
            }
        )

        this.socketService.getLastMessage().subscribe(
            (response) => {
                if (response !== "" ){
                    this.popUp(JSON.parse(response.toString()),true); 
                }
            },
            (error) =>{ console.log("PB SOCKET") }// Auth Failed
        );
    }

    /*
    * BackEnd Request on logout route in order to destroy current mongoDBSession
    */
    logout(): void{
        this.userService.logout();
    }



    // Init web socket connection and try to restore previous user session
    constructor(public fb: FormBuilder, 
                private userService: UserService, 
                private quizzService: QuizzService, 
                private socketService: SocketService,
                private router: Router) {
        this.form = this.fb.group({
            username: [''],
            password: ['']
        });        
        
        this.userService.login({}).subscribe(
            (response) => {
                this.authFailed = false;
                this.popUp("Session restored : " + localStorage.getItem('lastConnexion'),true);
                this.userService.userLogic(response);
            },
            (error) =>{ }// Auth Failed
        );
    }

    // Shows the  notifcation popup defined in app.compoment.html 
    // With a message and a color according to arg['type']
    popUp(popupMsg,type):void {
        this.notifMsg = popupMsg;
        var popup = document.getElementById("notifPopup");

        if (!type)
            this.popColor = {'background-color' : 'red'};
        else
            this.popColor = {'background-color' : 'green'};
        
       popup.classList.add("show");
    }

    // Hide notification popup defined in app.compoment.html
    popDown():void {
        var popup = document.getElementById("notifPopup");
        popup.classList.remove("show");
    }
        
    /*
    * BackEnd Request with userService.login()
    *   sends credentials to backend service
    *       clear username 
    *       sha1(password)
    *   gets from backend service
    *       User OBJECT ( ie : /client/app/user.ts)
    *
    */
    submitForm(){
        this.authFailed = false;
        var credentials = {
            username:this.form.get('username').value,
            password:sha1(this.form.get('password').value)
        }
        this.userService.login(credentials).subscribe(
            (response) => {
                this.authFailed = false;
                var lastCo = localStorage.getItem('lastConnexion');
                if ( lastCo )
                    this.popUp("Previous session : " + lastCo,true);
                else
                    this.popUp("Welcome into CERI-Quizz\n You can set a picture and a biography in your profile. ",true);
                this.userService.userLogic(response);
            },
            (error) =>{
                this.authFailed = true,
                this.popUp("Auth Failed ! Try Again ",false);
                console.log(error);
            }// Auth Failed
        );
    }


}

/* 
        Author: RAYMONDAUD Quentin
        You can do what you want with this code 
*/

import { Component,Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Quizz } from '../interfaces/quizz';
import {QuizzService } from '../httpservices/quizz.service';

@Component({
    selector: 'app-quizz-detail',
    templateUrl: './quizz-detail.component.html',
    styleUrls: ['./quizz-detail.component.css']
})

export class QuizzDetailComponent implements OnInit {
    @Input() private theme: String;       // Theme passed quizz.component
    @Input() private difficulty: String   // Difficulty selected on quizz.component
    @Output() private quizzEventOut = new EventEmitter<string>();
    private quizz: Quizz;                 // Quizz received by backend on component init

    // construct component and services
    constructor(private quizzService: QuizzService) {}

    // Init component
    ngOnInit(): void {
        this.getQuizzByTheme();
    }

    // Receive event from chidren component
    quizzEventReceiver(eventMsg: string): void{
        //this.ngOnInit();
        console.log("Quizz-Detail received => " + eventMsg);
        if (eventMsg === 'replay')
            this.ngOnInit();
        if (eventMsg === 'quizz')
            this.quizzEventEmitter(eventMsg);
    }

    // Sends event to father component
    quizzEventEmitter(value: string) {
        this.quizzEventOut.emit(value);
    }

    isQuizz(){
        return ( this.quizz != undefined );
    }

    // Request the backend for a quizz with precise theme and difficulty level
    // Fill a Quizz object and pass its questions to question.details.component
    getQuizzByTheme(): void{
        this.quizzService.getQuizzByTheme(this.theme, this.difficulty)
                .subscribe(quiz =>{
                        this.quizz = quiz;
                        //console.log(quiz)
                }
        );
    }
}

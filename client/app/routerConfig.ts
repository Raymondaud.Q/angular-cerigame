// routerConfig.ts
// Author Raymondaud Quentin

// Module that route view in client side 

import { Routes } from '@angular/router';
import { QuizzComponent } from './quizz/quizz.component';
import { UserComponent } from './user/user.component';
import { GameComponent } from './game/game.component';
import { HomePanelComponent } from './home-panel/home-panel.component';
import { ChallengeComponent } from './challenge/challenge.component';

const appRoutes: Routes = [

  {
    path: '',
    component: HomePanelComponent
  },
  { // User profile view
    path: 'user/:id/:editable',
    component: UserComponent
  }, 
  { path: 'challenge/:idChallenge', 
    component: ChallengeComponent 
  },
  // Quizz theme selection view
  { path: 'quizz', 
    component: QuizzComponent 
  },
    { path: 'game', 
    component: GameComponent
  },

];
export default appRoutes;
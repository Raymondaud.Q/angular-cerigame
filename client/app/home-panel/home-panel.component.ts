// RAYMONDAUD Quentin

import { Component, OnInit } from '@angular/core';
import { UserListComponent } from '../user-list/user-list.component';
import { RankingComponent } from '../ranking/ranking.component';
import { UserService } from '../httpservices/user.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-home-panel',
  templateUrl: './home-panel.component.html',
  styleUrls: ['./home-panel.component.css']
})

// Home Panel Wrapper Component
export class HomePanelComponent implements OnInit {

	userList: User[];  // UserList from userService
	subscription: any; // UserList Subscription 

	constructor(private userService: UserService) { }

	// Fetch userlist from backend
	ngOnInit(): void {
		this.userService.getUserList();
		this.subscription = this.userService.userList.subscribe(list=>{
			this.userList = list;
		})
	}

	// Remove the subscription when destroying the component
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}

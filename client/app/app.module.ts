import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { QuizzComponent } from './quizz/quizz.component';
import { QuizzDetailComponent } from './quizz-detail/quizz-detail.component';
import { QuestionDetailComponent, ChallengeDialog } from './question-detail/question-detail.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { GameComponent } from './game/game.component';
import { UserService } from './httpservices/user.service';
import { QuizzService } from './httpservices/quizz.service';
import { SocketService , ChoiceDialog }  from './httpservices/socket.service';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import appRoutes from './routerConfig';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { ResultPreviewComponent, AnecdoteDialog } from './result-preview/result-preview.component';
import { MatDialogModule } from '@angular/material/dialog';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import { UserComponent } from './user/user.component';
import { UserStatComponent } from './user-stat/user-stat.component';
import { UserHistoryComponent } from './user-history/user-history.component';
import { UserListComponent } from './user-list/user-list.component';
import { HomePanelComponent } from './home-panel/home-panel.component';
import { ChallengeComponent } from './challenge/challenge.component';
import { RankingComponent } from './ranking/ranking.component';
import { UserChallengeHistoryComponent } from './user-challenge-history/user-challenge-history.component';

@NgModule({
  entryComponents: [ResultPreviewComponent, AnecdoteDialog, QuestionDetailComponent, ChallengeDialog, ChoiceDialog ],
  declarations: [
    AppComponent,
    QuizzComponent,
    QuizzDetailComponent,
    QuestionDetailComponent,
    UserDetailComponent,
    GameComponent,
    ResultPreviewComponent,
    AnecdoteDialog,
    ChallengeDialog,
    ChoiceDialog,
    UserComponent,
    UserStatComponent,
    UserHistoryComponent,
    UserListComponent,
    HomePanelComponent,
    ChallengeComponent,
    RankingComponent,
    UserChallengeHistoryComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(appRoutes,  { useHash: true }),
    MatCardModule,
    MatRadioModule,
    MatDialogModule
  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
    UserService,
    QuizzService,
    SocketService
  ],
  bootstrap: [AppComponent]
})  
export class AppModule { }
  
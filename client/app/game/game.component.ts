import { Component, OnInit, ElementRef, Renderer2} from '@angular/core';
import * as P5 from 'p5';
import * as Matter from 'matter-js'



@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
    game: P5;
    Engine = Matter.Engine;
    Render = Matter.Render;
    World = Matter.World;
    Bodies = Matter.Bodies;
    boxA = this.Bodies.rectangle(400, 200, 80, 80);
    ballA = this.Bodies.circle(380, 100, 40);
    ballB = this.Bodies.circle(460, 10, 40);
    ground = this.Bodies.rectangle(400, 380, 810, 60, { isStatic: true });
    width: Number;
    height: Number
    mConstraint: any;
    engine: any;
    mouse: any;
    canvas:any;

    constructor( private el: ElementRef, private renderer: Renderer2 ) { }

    ngOnInit() {
       

        this.game = new P5(p => {
            let x = 100;
            let y = 100;
            console.log(this.height)
            p.setup = () => {
                /*
                this.width = document.getElementById('body').offsetWidth//includes margin,border,padding
                this.height = document.getElementById('body').offsetHeight - document.getElementById('navbar').offsetHeight;//includes margin,borde
                */
                this.width = p.windowWidth;
                this.height = p.windowHeight;
                this.canvas = p.createCanvas(this.width, this.height);
                this.engine = this.Engine.create();
                this.mouse = Matter.Mouse.create(this.canvas.elt);
                this.mouse.pixelRatio = p.pixelDensity();
                this.mConstraint = Matter.MouseConstraint.create(this.engine, {mouse:this.mouse});
                this.World.add(this.engine.world, [this.boxA, this.ballA, this.ballB, this.ground, this.mConstraint]);
                //this.Engine.run(this.engine); 

            };

            p.windowResized = () => {
                this.width = document.getElementById('body').offsetWidth;//includes margin,border,padding
                this.height = document.getElementById('body').offsetHeight - document.getElementById('navbar').offsetHeight;
                p.resizeCanvas(this.width, this.height);
            }

            p.init = () => {
                
            }

            p.draw = () => {
                p.background(0);
                p.fill(255);
                p.rect( this.ballA.position.x, this.ballA.position.y , 50, 50);
                Matter.Engine.update(this.engine);
                
            };
        }, this.el.nativeElement);

    }

}

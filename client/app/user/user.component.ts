/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit } from '@angular/core';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { UserStatComponent } from '../user-stat/user-stat.component';
import { UserHistoryComponent } from '../user-history/user-history.component';
import { UserChallengeHistoryComponent } from '../user-challenge-history/user-challenge-history.component';
import { Router, ActivatedRoute  } from '@angular/router';
import { UserService } from '../httpservices/user.service';
import { User } from '../interfaces/user';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})

// Component that holds other component related to user informations
export class UserComponent implements OnInit {

    userId : String;
    editable : String;
    userList : User[];
    subscription : any;

    constructor( private route: ActivatedRoute, private router: Router, private userService: UserService) {     
        this.route.params.subscribe(params => {
                this.userId= this.route.snapshot.paramMap.get('id');
                this.editable=  this.route.snapshot.paramMap.get('editable');
                this.userService.getUserList();
                this.subscription = this.userService.userList.subscribe(list=>{
                    this.userList = list;
                })
        });
    }

    ngOnInit(): void {  }
}


/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit, Input } from '@angular/core';
import { User } from '../interfaces/user';
import { UserService } from '../httpservices/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

// Component that dispays the userService's userListe

export class UserListComponent implements OnInit {

	@Input() userList : User[];

	displayOffLine = true;

	constructor(private userService: UserService) {	}

	swapMode(){
		console.log("OK")
		this.displayOffLine = ! this.displayOffLine;
		this.ngOnInit();
	}

	ngOnInit(): void { 	}

	// listener
	onSelect(user):void{ } 

}

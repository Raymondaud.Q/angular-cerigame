/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { RankingService } from '../httpservices/ranking.service';
import { UserService } from '../httpservices/user.service';
import { User } from '../interfaces/user';
import { PlayerRank } from '../interfaces/rank';
import { BrowserModule } from '@angular/platform-browser'

@Component({
	selector: 'app-ranking',
	templateUrl: './ranking.component.html',
	styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

	ranking : PlayerRank[];
	@Input() userList : User[];
	rankedUsers : User[];
	subscription: any;

	constructor( private rankingService : RankingService,
				 private userService : UserService ) { }

	// Reinit the component on data change
	ngOnChanges(): void {
		if ( this.subscription )
			this.subscription.unsubscribe();
		this.ngOnInit();
	}

	// Init the ranking array in order to display the table
	// subscribe to a service's Observable in order tu update data
	ngOnInit(): void {
		this.subscription = this.rankingService.getRanking().subscribe( ranks => { 
			this.ranking = ranks;
			let size = ranks.length;
			this.rankedUsers = [];
			for ( let i = 0; i < size ; i++){
				if ( ranks[i] != null ){
					let rank = ranks[i];
			        if ( this.userList )
					{
					    for ( let user of this.userList ){
					        if (user.id == rank.idUser )
					            this.rankedUsers[i] = user;
					    }
					}
				}
			}
			
		});
	}

	// Removes subscription when destroying component
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}

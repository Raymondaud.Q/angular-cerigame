import { Quizz } from './quizz';
import { Question } from './question';


export const QUIZZ: Quizz[] = [

{
  id: 1,
  theme: "Héros Marvel",
  redacteur: "Philippe Bresoux",
  fournisseur : "OpenQuizzDB - Fournisseur de contenu libre (www.openquizzdb.org)",
  quizz : [{
    "id" : 1,
    "question" : "Dans quelles aventures retrouve-t-on les personnages de Loïs et Clark ?",
    "propositions" : [
      "Hulk",
      "Spiderman",
      "Superman",
      "Deadpoil"
    ],
    "reponse" : "Superman",
    "anecdote" : "Dans 'Superman', Clark Kent et Loïs Lane, deux des héros de la saga, sont journalistes au 'Daily Planet'."
  },
  {
    "id" : 2,
    "question" : "Dans les X-Men, quelle substance constitue le squelette de Wolverine ?",
    "propositions" : [
      "Vibranium",
      "Neutronium",
      "Cavorite",
      "Smegma"
    ],
    "reponse" : "Adamantium",
    "anecdote" : "L'adamantium, alliage de métal imaginaire le plus résistant de tous les métaux imaginaires connus, ne s'oxyde pas et ne rouille pas."
  }]
},

{
  id: 2,
  theme: "Star Wars",
  redacteur: "Philippe Bresoux",
  fournisseur : "OpenQuizzDB - Fournisseur de contenu libre (www.openquizzdb.org)",
  "quizz" : [
      {
        "id" : 1,
        "question" : "Dans quelles aventures retrouve-t-on les personnages de Loïs et Clark ?",
        "propositions" : [
          "Batman",
          "Spiderman",
          "Superman",
          "Bluepill"
        ],
        "reponse" : "Superman",
        "anecdote" : "Dans 'Superman', Clark Kent et Loïs Lane, deux des héros de la saga, sont journalistes au 'Daily Planet'."
      },
      {
        "id" : 2,
        "question" : "Dans les X-Men, quelle substance constitue le squelette de Wolverine ?",
        "propositions" : [
          "Vibranium",
          "Adamantium",
          "Cavorite",
          "Lentilles"
        ],
        "reponse" : "Adamantium",
        "anecdote" : "L'adamantium, alliage de métal imaginaire le plus résistant de tous les métaux imaginaires connus, ne s'oxyde pas et ne rouille pas."
      }]
  },

{
  id: 3,
  theme: "Culture générale 4 (La culture, c'est l'expression du vivant)",
  redacteur: "Philippe Bresoux",
  fournisseur : "OpenQuizzDB - Fournisseur de contenu libre (www.openquizzdb.org)",
  quizz : [{
    "id" : 1,
    "question" : "Dans quelles aventures retrouve-t-on les personnages de Loïs et Clark ?",
    "propositions" : [
      "Hulk",
      "Batman",
      "Spiderman",
      "Superman"
    ],
    "reponse" : "Superman",
    "anecdote" : "Dans 'Superman', Clark Kent et Loïs Lane, deux des héros de la saga, sont journalistes au 'Daily Planet'."
  },
  {
    "id" : 2,
    "question" : "Dans les X-Men, quelle substance constitue le squelette de Wolverine ?",
    "propositions" : [
      "Vibranium",
      "Neutronium",
      "Adamantium",
      "Cavorite"
    ],
    "reponse" : "Adamantium",
    "anecdote" : "L'adamantium, alliage de métal imaginaire le plus résistant de tous les métaux imaginaires connus, ne s'oxyde pas et ne rouille pas."
  }]
},

{
  id: 4,
  theme:   "Actu people : août 2018 (Ils ont fait l'actualité)",
  redacteur: "Philippe Bresoux",
  fournisseur : "OpenQuizzDB - Fournisseur de contenu libre (www.openquizzdb.org)",
  quizz : [{
    "id" : 1,
    "question" : "Dans quelles aventures retrouve-t-on les personnages de Loïs et Clark ?",
    "propositions" : [
      "Hulk",
      "Batman",
      "Spiderman",
      "Superman"
    ],
    "reponse" : "Superman",
    "anecdote" : "Dans 'Superman', Clark Kent et Loïs Lane, deux des héros de la saga, sont journalistes au 'Daily Planet'."
  },
  {
    "id" : 2,
    "question" : "Dans les X-Men, quelle substance constitue le squelette de Wolverine ?",
    "propositions" : [
      "Vibranium",
      "Neutronium",
      "Adamantium",
      "Cavorite"
    ],
    "reponse" : "Adamantium",
    "anecdote" : "L'adamantium, alliage de métal imaginaire le plus résistant de tous les métaux imaginaires connus, ne s'oxyde pas et ne rouille pas."
  }]
},
{
  id: 5,
  theme:   "Trouvez le nombre",
  redacteur: "Philippe Bresoux",
  fournisseur : "OpenQuizzDB - Fournisseur de contenu libre (www.openquizzdb.org)",
  quizz : [{
    "id" : 1,
    "question" : "Dans quelles aventures retrouve-t-on les personnages de Loïs et Clark ?",
    "propositions" : [
      "Hulk",
      "Batman",
      "Spiderman",
      "Superman"
    ],
    "reponse" : "Superman",
    "anecdote" : "Dans 'Superman', Clark Kent et Loïs Lane, deux des héros de la saga, sont journalistes au 'Daily Planet'."
  },
  {
    "id" : 2,
    "question" : "Dans les X-Men, quelle substance constitue le squelette de Wolverine ?",
    "propositions" : [
      "Vibranium",
      "Neutronium",
      "Adamantium",
      "Cavorite"
    ],
    "reponse" : "Adamantium",
    "anecdote" : "L'adamantium, alliage de métal imaginaire le plus résistant de tous les métaux imaginaires connus, ne s'oxyde pas et ne rouille pas."
  }]
}

];
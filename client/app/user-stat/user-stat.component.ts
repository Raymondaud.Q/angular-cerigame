import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { UserService } from '../httpservices/user.service';
import { PlayerStat } from '../interfaces/player-stat';

@Component({
    selector: 'app-user-stat',
    templateUrl: './user-stat.component.html',
    styleUrls: ['./user-stat.component.css']
})
export class UserStatComponent implements OnInit {
    @Input() userid: String;// Str PgSql user id
    stat: PlayerStat;
    constructor(private userService: UserService) { }
    
    // Reinit component on data change
    ngOnChanges(): void{
        this.ngOnInit();
    }

    // Init component data for displaying user stats
    ngOnInit(): void {
        if ( this.userid)
            this.userService.getPlayerStat(this.userid).subscribe(
                stats => {
                    this.stat = stats;
                }
            );
    }

}

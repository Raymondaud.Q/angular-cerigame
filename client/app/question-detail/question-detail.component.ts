/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component,  Output, EventEmitter, OnInit, Input, Inject, OnChanges } from '@angular/core';
import { QuizzService } from '../httpservices/quizz.service';
import { UserService } from '../httpservices/user.service';
import { Quizz } from '../interfaces/quizz';
import { User } from '../interfaces/user';
import { Question } from '../interfaces/question';
import { Response } from '../interfaces/response';
import { QuizzResponse } from '../interfaces/quizz-response';
import { QuizzResult } from '../interfaces/quizz-result';
import { ResultPreviewComponent } from '../result-preview/result-preview.component';
import { Router } from '@angular/router';
import { MatDialog, MatDialogModule, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  userList: User[];
  quizz: Quizz;
  correction: QuizzResult;
}

@Component({
  selector: 'challenge-dialog',
  templateUrl: './challenge-dialog.html',
})

// Dialog that permits to challenge another player
export class ChallengeDialog {
    private currentUser: User;

    constructor(@Inject(MAT_DIALOG_DATA)    public data: DialogData,
                                            private userService: UserService,
                                            private quizzService: QuizzService) {}

    onSelect(user:User) {
        this.currentUser = user;
        this.quizzService.challengeUser(this.data.quizz, user.id, this.data.correction.score ).subscribe(resp =>{
            console.log(resp);
        });

    }

    ngOnInit() {
        // Add an event listener to window
        // Window can be defined in the pollyfiles.ts as: 
        // if (window) {
        //    (window as any).global = window;
        // }
        window.addEventListener('scroll', this.scroll, true); //third parameter
        this.userService.userList.subscribe(list=>{
            this.data.userList = list;
        })
    }

    ngOnDestroy() {
        window.removeEventListener('scroll', this.scroll, true);
    }

    scroll = (event: any): void => {
        // Here scroll is a variable holding the anonymous function 
        // this allows scroll to be assigned to the event during onInit
        // and removed onDestroy
        // To see what changed:
        const number = event.srcElement.scrollTop;
        if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight){  }
    };
} 

@Component({
    selector: 'app-question-detail',
    templateUrl: './question-detail.component.html',
    styleUrls: ['./question-detail.component.css']
})


// Question component
// Displays question and results after the last question 
export class QuestionDetailComponent implements OnInit {

    @Input() private quizz: Quizz;         // Quizz passed by quizz.details.comp
    @Input() private theme: string;        // theme passed by quizz.details.comp
    @Input() private isChallenge : any;    // theme passed by quizz.details.comp
    @Output() private quizzEventOut = new EventEmitter<string>();

    private questionIndex = 0;          // Current question index
    questionNumber : number;
    private responses: Response[];      // Array of current quizz user's responses
    private timeElapsed: number;
    private interval;
    private correction: QuizzResult;
    private targetUser: number;
    private userList: User[];
    private alreadyChallenged: Boolean;

    startTimer() {
        //console.log("OK")
        this.interval = setInterval(() => {
            this.timeElapsed += 0.05;
        },50)
    }

    pauseTimer() {
        clearInterval(this.interval);
    }

    getQI() {
        return this.questionIndex;
    }

    getElapsedTime() {
        return this.timeElapsed;
    }


    // Emit 'replay', 'home' or 'challenge' according to the pressed button 
    // Usefull to transfer data or trigger event in parent component
    quizzEventEmitter(value: string) {

        if ( value === 'replay' || value === 'quizz'  ){
            this.quizzEventOut.emit(value);
            this.init();
        }
        else if ( value === 'home'){
            this.router.navigateByUrl("/");
        }
        else if ( value === 'challenge'){
            if ( !this.isChallenge && ! this.alreadyChallenged ) {
                this.alreadyChallenged = true;
                this.dialog.open(ChallengeDialog, {
                    data: {
                        quizz: this.quizz, 
                        correction: this.correction,
                        userList: this.userService.getUserList()
                    }
                });
            }
        }
    }  


    // Init services
    constructor(private quizzService: QuizzService, 
                private userService: UserService, 
                private router: Router, 
                public dialog: MatDialog) { }

    // Init component
    ngOnInit(): void {
        this.init();
    }

    ngOnChanges(): void{
        this.init();
    }

    // Component init process
    init(): void{
        if ( this.quizz && this.quizz.quizz){
            this.responses = new Array<Response>();
            this.questionIndex = 0 ;
            this.timeElapsed = 0;
            this.targetUser = -1;
            this.alreadyChallenged = false;
            this.questionNumber = this.quizz.quizz.length
            if (this.interval)
                clearInterval(this.interval);
            
            this.startTimer();
        }
    }

    // Function triggered when user has clicked on a proposition button 
    // Save the response and shows next question
    next(event, responseQ){
        const response: Response = {    theme: this.theme,
                                        idQuestion: this.quizz.quizz[this.questionIndex].id, 
                                        response: responseQ };
        this.responses.push(response);
        this.questionIndex ++;
        this.userService.user.subscribe(user => {
            if( this.questionIndex == this.questionNumber){
                this.pauseTimer();
                const quizzresponses: QuizzResponse = { responses: this.responses,
                                                        elapsedTime: this.getElapsedTime(),
                                                        idPostGre: user.id,
                                                        challengeId: this.isChallenge,
                                                        difficulty: this.quizz.quizz[0].propositions.length-1};
                this.quizzService.getResults(quizzresponses).subscribe(resp =>{
                    this.correction = resp;
                });                
            }
        });
    }
}

/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit } from '@angular/core';
import { Quizz } from '../interfaces/quizz';
import { QuizzService } from '../httpservices/quizz.service';

@Component({
  selector: 'app-quizz',
  templateUrl: './quizz.component.html',
  styleUrls: ['./quizz.component.css']
})

export class QuizzComponent implements OnInit {

    private selectedTheme: String; // Filled by the value of the li element clicked
    private themes: String[];      // List of theme received from the server
    private difficulty: Number=2;  // Default difficulty

    // Construct component and services
    constructor( private quizzService: QuizzService ) {}

    // Init component
    ngOnInit(){
        this.selectedTheme = "";
        this.getThemes();
    }

    // Receive event from chidren component
    quizzEventReceiver(eventMsg: String): void{
        //this.ngOnInit();
        console.log("Quizz received => " + eventMsg);
        if (eventMsg === 'quizz')
            this.selectedTheme = undefined;
    }

    isThemeUndefined(){
        return ( this.themes == undefined );
    }

    getSelectedTheme(){
        return this.selectedTheme;
    }

    // Backend Request 
    // Receive list of themes from the server
    getThemes(): void {
        this.quizzService.getThemes();
        this.quizzService.themeList.subscribe( themes => {
            this.themes = themes;
        })
        
    }

    // Function triggered on li element Click
    onSelect(theme: String): void {
      this.selectedTheme = theme;
    }
}



/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { UserService } from '../httpservices/user.service';
import { QuizzResult } from '../interfaces/quizz-result';
import { PgResult } from '../interfaces/postgre-result';

@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.css']
})
export class UserHistoryComponent implements OnInit {
    @Input() userid: String;    // Str PgSql user id
    history: QuizzResult[];
    hist: QuizzResult;
    pgHistory: PgResult[];
    historyMode = false;
    constructor(private userService: UserService) { }
    
    ngOnChanges(): void{
        this.ngOnInit();
    }
    // fetch user history from backend
    ngOnInit(): void {  
        if ( this.userid){
            // Mon système
            this.userService.getPlayerHistory(this.userid,0,10).subscribe(
                hist => {
                    this.history = hist;
                }
            );
            // PG system ( affiché que si pas de données dans le mien )
            this.userService.getPgPlayerHistory(this.userid,0,10).subscribe(
                pgHist => {
                    this.pgHistory = pgHist;
                }
            );
        }
    }

    // details of my personnal history system
    getSelectedHistory(): QuizzResult {
        return this.hist;
    }

    swapHistoryMode(): void{
        this.historyMode = !this.historyMode;
    }

    // listenner of seletec history that we want to show details
    onSelect(history){
        if ( this.hist === history )
            this.hist = undefined ;
        else
            this.hist = history;
    }

}

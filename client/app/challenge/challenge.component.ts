// RAYMONDAUD Quentin

import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { QuizzService } from '../httpservices/quizz.service';

@Component({
  selector: 'app-challenge',
  templateUrl: './challenge.component.html',
  styleUrls: ['./challenge.component.css']
})

// Challenge quizz view
export class ChallengeComponent implements OnInit {

	challengeId : any; // MongoDb object ID
	idChallHist : any; // Historique row id to upsert if quizz finished before timeout
	challengeInfo : any; // Quizz and result of previous user
	challenge = "Challenge"
	@Output() private quizzEventOut = new EventEmitter<string>();

	// Init services and router
	constructor( 	private route: ActivatedRoute,
					private router: Router,
					private quizzService: QuizzService ) {
	}
    
    // Init data in order to display questions
	ngOnInit(){
		this.route.params.subscribe(params => {
		    this.challengeId= this.route.snapshot.paramMap.get('idChallenge');
		});
		this.quizzService.getChallengeById(this.challengeId, true).subscribe(result =>{
	    	this.challengeInfo = result[0];
	    	this.challenge = result[0].quizz.theme;
	    	console.log(result);
	    })
	}

	// Receive event thrown by button at the end of a quizz
    quizzEventReceiver(eventMsg: string): void{
            //this.ngOnInit();
            console.log("Quizz-Detail received => " + eventMsg);
            if (eventMsg === 'replay')
                this.ngOnInit();
            if (eventMsg === 'quizz')
                this.quizzEventEmitter(eventMsg);
    }

    // Emit function
    quizzEventEmitter(value: string) {
            this.quizzEventOut.emit(value);
    }

}

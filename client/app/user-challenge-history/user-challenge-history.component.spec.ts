import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserChallengeHistoryComponent } from './user-challenge-history.component';

describe('UserChallengeHistoryComponent', () => {
  let component: UserChallengeHistoryComponent;
  let fixture: ComponentFixture<UserChallengeHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserChallengeHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserChallengeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

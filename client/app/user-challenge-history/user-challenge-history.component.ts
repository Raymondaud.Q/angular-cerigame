/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ChallengeResult } from '../interfaces/challenge-result';
import { UserService } from '../httpservices/user.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-user-challenge-history',
  templateUrl: './user-challenge-history.component.html',
  styleUrls: ['./user-challenge-history.component.css']
})


// Displays the medal list
// Provide a button to visit user that the current user won 
export class UserChallengeHistoryComponent implements OnInit {
	@Input() userid : String;
	@Input() userList : User[];
	challengeHistory : ChallengeResult[];
	challengedUser : User[];
	nbMedal : number;
	constructor(private userService: UserService ) { }

	// Reinit display on data change
    ngOnChanges(): void{
        this.ngOnInit();
    }

    // Init component data for displaying medals and users
	ngOnInit(): void {
		this.challengedUser = [];
		if ( this.userid && this.userList){
			this.userService.getPlayerChallengeHistory(this.userid, 0, 1000).subscribe(
				hist => {
					this.challengedUser = [];
					this.challengeHistory = hist;
					this.nbMedal = 0;
					let size = this.challengeHistory.length;
					for ( let i = 0 ; i < size ; i ++){
						let idUser =  this.challengeHistory[i].looser;
						for ( let user of this.userList ) {
							if ( idUser == user.id && idUser != parseInt(this.userid.toString()) ){
								this.challengedUser[i] = user;
								this.nbMedal++;
							}
						}
					}
				}
			);
		}
	}

}

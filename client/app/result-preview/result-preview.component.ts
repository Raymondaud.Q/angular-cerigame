/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit, Input, Inject, OnChanges } from '@angular/core';
import { QuizzResult } from '../interfaces/quizz-result';
import { MatDialog, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '../interfaces/user';
import { UserService } from '../httpservices/user.service';

export interface DialogData {
  message: String;
}
 
@Component({
    selector: 'app-result-preview',
    templateUrl: './result-preview.component.html',
    styleUrls: ['./result-preview.component.css']
})

// Component that dipays result received by a client after a quizz or a challeneg
export class ResultPreviewComponent implements OnInit {

    @Input() correction: QuizzResult;         // Quizz passed by quizz.details.comp
    winner : User;
    
    // Init service and dialog builder
    constructor(public dialog: MatDialog,
                private userService: UserService) { }

    // Reinit Component on data change
    ngOnChanges(): void{
        this.ngOnInit();
    }

    // Set the winner if it's a challenge
    ngOnInit(): void {
        if ( this.correction && this.correction.idWinner ){
            this.winner = this.userService.getLocalUserById(this.correction.idWinner);
        }
    }

    // Open the anecdote dialog
    openDialog(anecdote: String) {
        console.log(anecdote);
        this.dialog.open(AnecdoteDialog, {
          data: {
            message: anecdote
          }
        });
    }
}

@Component({
    selector: 'anecdote-dialog',
    templateUrl: './anecdote-dialog.html',
})
export class AnecdoteDialog {
    constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
}
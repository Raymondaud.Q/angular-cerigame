/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { UserService } from '../httpservices/user.service';
import { User } from '../interfaces/user';
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
    @Input() userid: String;                            // Str PgSql user id
    @Input() editable: String;                          // Current user profile or not ?
    userData: { userData: User, editable: Boolean };    // Subject
    mode: Number = 0;                                   // Determine if the diplayed template show Form or not
    profileForm: FormGroup;                             // Form
    picSave: string;                                    // Save for discard change
    bioSave: string;                                    // Save for discard

    constructor(public fb: FormBuilder, private userService: UserService) {
    	this.profileForm = this.fb.group({
            url: [''],
            bio: ['']
        })
    }
    ngOnChanges(): void{ this.ngOnInit(); }
    // Init compoment 
    // Request Node API on /user/:id
    // editable is client side computed value that indicate 
    // if the client can show edit form or not
    ngOnInit(): void {
        //console.log("USER-DETAILVIEW " + this.userid );
        if ( this.userid)
            this.userService.getUserById(this.userid).subscribe(
                usr => {
                    this.userData = {userData: usr, editable: (Number(this.editable) > 0 )};
                    this.picSave = usr.avatar; this.bioSave= usr.humeur;
                    //console.log(this.userData);
                }
            );
    }

    // Show edit fields
    editMode(): void { this.mode = 1; }

    // No edit fields
    viewMode(): void {  this.mode = 0;
                        this.userData.userData.avatar = this.picSave;
                        this.userData.userData.humeur = this.bioSave; }

    // Requests the backend for editing Bio and or avatar
    submit(): void {
    	let editedProfile = {
            imgUrl: this.profileForm.get('url').value,
            bio: this.profileForm.get('bio').value
        }
        this.userService.updateProfile(editedProfile).subscribe(
            (usr) => {
                this.userData = {userData: usr, editable: (Number(this.editable) > 0 )};
                this.picSave = usr.avatar; this.bioSave= usr.humeur;
                console.log(this.userData);
            },
            (error)=> {
                this.userData.userData.avatar = this.picSave;
                this.userData.userData.humeur = this.bioSave;
            }
        );
        this.mode = 0;
    }
  

   
}

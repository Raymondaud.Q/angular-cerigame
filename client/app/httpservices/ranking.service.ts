// RAYMONDAUD Quentin

import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { PlayerRank } from '../interfaces/rank';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

// Exportable Ranking Service module
export class RankingService {

	_data : PlayerRank[] = undefined;
	private _ranking = new BehaviorSubject<PlayerRank[]>(this._data);
    ranking = this._ranking.asObservable(); 

    // Update the service's rank array
    updateRanking( playerR: PlayerRank[]){
    	this._data = playerR;
        this._ranking.next(this._data);
    }

    // Request the server in order to get the current rank array
    getRanking(){
    	if ( this._data != undefined ){
       		return this.ranking;
    	}
    	else {
    		var resp = this.http.get<PlayerRank[]>( "/ranking");
    		resp.subscribe( rank => {
    			this._data = rank;
    			this._ranking.next(this._data);
    		} );
    		return resp;
    	}
    }

	constructor( private http : HttpClient ) {}
}

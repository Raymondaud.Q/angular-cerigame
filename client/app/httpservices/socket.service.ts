// RAYMONDAUD Quentin

import { Observable, BehaviorSubject, of } from 'rxjs';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {environment} from "../../environments/environment";
import { UserService } from './user.service';
import { QuizzService } from './quizz.service';
import { RankingService } from './ranking.service';
import { Component, Inject, Injectable } from '@angular/core';
import { MatDialog, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute  } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SocketService {

    socketUrl = environment.backendSocketKey;
    myWebSocket: WebSocketSubject<Object> = webSocket(this.socketUrl);
    private _lastMessage = new BehaviorSubject<String>('');       // User object that can be observed to provide
    lastMessage = this._lastMessage.asObservable();

    // Init services and dialog builder
  	constructor( private userService: UserService,
                 private rankingService: RankingService,
                 public dialog: MatDialog ) { 
        // Web socket test
        this.myWebSocket.subscribe(    
            msg => this.updateLastMessage(msg), 
            // Called whenever there is a message from the server    
            // err => console.log(err), 
            // Called if WebSocket API signals some kind of error    
            () => console.log('complete') 
            // Called when connection is closed (for whatever reason)  
        );
    }

    // Socket server controller
    updateLastMessage( tl ){
        //console.log(tl.type + " recu ! ");
        if ( tl.type === "USER"){
            this.userService.updateUserInList(tl.userDetail);
            this._lastMessage.next(JSON.stringify(tl.message));
        }
        else if ( tl.type ==="DEFI"){
            if ( this.userService.data.id == tl.target){
                // console.log("VOUS ETES DEFIE !")
               let user = this.userService.getLocalUserById(tl.src);
                this.dialog.open(ChoiceDialog, {
                    disableClose: true,
                    data: {
                        message: user.nom + " " + user.prenom + " challenged you ! You want to fight ? ",
                        info: tl
                    }
                });
            }
            this._lastMessage.next(JSON.stringify(tl.message));
        }
        else if ( tl.type ==="RANKING"){
            this.rankingService.updateRanking(tl.ranking);
        }
        
    }

    // Return lastMessage received by the client
    getLastMessage(){
        return this.lastMessage;
    }

    // Push a message to server
    emit(msg){
    	this.myWebSocket.next(msg);
    }
}

@Component({
    selector: 'challenge-dialog',
    templateUrl: './challenge-dialog.html',
})

// Socket service's dialog component
export class ChoiceDialog {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                        private quizzService: QuizzService,
                        private route: ActivatedRoute,
                        private router: Router) {}

    // Retrieve to server the user choice
    challengeChoice(choice = false ){
        //console.log( choice + " Your choice");  
        if ( choice )
            this.router.navigate(['challenge', this.data.info.idChallenge]);
        else // Deny and lose the chall
            this.quizzService.getChallengeById(this.data.info.idChallenge, false).subscribe(result =>{})
    }
}
/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Quizz } from '../interfaces/quizz';
import { QuizzResponse } from '../interfaces/quizz-response';
import { QuizzResult } from '../interfaces/quizz-result';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QuizzService {
    private quizzUrl =  environment.backendBaseKey+'/quizz';                // Base str for quizz request URI
    private verifQuizzUrl =  environment.backendBaseKey+'/quizz/verify';    // Base str for quizz response request URI
    private themesUrl =  environment.backendBaseKey+'/themes';              // Base str for Theme request UIR
    private challengeUrl =  environment.backendBaseKey+'/quizz/challenge';  // Base str for Theme request UIR
    private themes: String[] = undefined;
    private _themeList = new BehaviorSubject<String[]>(this.themes);       // User object that can be observed to provide
    themeList = this._themeList.asObservable();                           // an unique instance of current user profile in every component that needs so

    updateThemeList( tl : String[]){
        this._themeList.next(tl);
    }

    constructor( private http: HttpClient ) { }


    // Requests the backend for a quizz with a specifed difficulty
    getQuizzByTheme(theme: String, difficulty: String): Observable<Quizz>{
    	return this.http.get<Quizz>( this.quizzUrl+ "/" +theme+"/"+difficulty);
    }

    // Requests the backend for the Theme list
    getThemesList(): any{
        var resp = this.http.get<String[]>(this.themesUrl);
        resp.subscribe(themes => {
            this.themes = themes 
            this.updateThemeList(this.themes);
        })
        return resp;
    }

    // get Quizz themes global service
    getThemes(): String[]{
        if ( this.themes )
            return this.themes
        else
            this.getThemesList();
    }

    // Sends quizz responses to backend and receive quizz results 
    getResults(responses: QuizzResponse): Observable<QuizzResult> {
        return this.http.post<QuizzResult>( this.verifQuizzUrl, responses);
    }
    // Sends a quizz and an user id to the server
    // In order to create a challenge
    challengeUser(challenge: Quizz, targetId: number, score: number): Observable<any> {
        return this.http.post<any>( this.challengeUrl, { quizz:challenge, targetUserId:targetId, score:score } );
    }

    // Requests the  backend for Challenge's Quizz by challengeID
    getChallengeById(idChallenge: number, choice: boolean): Observable<any>{
        return this.http.get<any>( this.challengeUrl+"/"+idChallenge+"/"+choice);
    }
}

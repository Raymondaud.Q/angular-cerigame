/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interfaces/user';
import { PlayerStat } from '../interfaces/player-stat';
import { QuizzResult } from '../interfaces/quizz-result';
//import { SocketService } from './socket.service';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
    data: User = undefined;                                     // User profile received at login
    dataList: User[] = undefined;
	logUrl =  environment.backendBaseKey+'/login';              // Base str for login request URI
    updateUrl =  environment.backendBaseKey+'/updateprofile';   // Base str for profile modification request URI
    userUrl = environment.backendBaseKey+'/user'                // Base str for user details request URI

    private _userList = new BehaviorSubject<User[]>(this.dataList); // User object that can be observed to provide
    userList = this._userList.asObservable(); 

    private _user = new BehaviorSubject<User>(this.data);       // User object that can be observed to provide
    user = this._user.asObservable();                           // an unique instance of current user profile in every component that needs so

    // Update the user observable
    updateUser( usr: User){
        this._user.next(usr);
    }

    // Update the userList observable
    updateUserList( usrl: User[]){
        this._userList.next(usrl);
    }

    // Update a user in the global userlist 
    updateUserInList(updatedUser){
        if ( ! this.dataList ){
            return this.getUserList()
        }
        else
            for ( let user of this.dataList){
                if ( user.id === updatedUser.id ){
                    this.dataList[this.dataList.indexOf(user)] = updatedUser;
                    this._userList.next(this.dataList); 
                }
            }
        return this.dataList;   
    }

    constructor( private http: HttpClient/*, private socketService: SocketService */) { }

    // Request api for login ( create or retrieve a session )
    login(cred): any {
        return this.http.post<User>( this.logUrl, cred);
    }
        
    // Request api for destroying current session
    // Uses Socket Services to notify disconnection
    logout(): void{
        
        this.http.get<any>(environment.backendBaseKey+'/logout').subscribe(data => 
            {
                //console.log(data);
                let prenom = this.data.prenom;
                let nom = this.data.nom;
                if ( data.destruct ){
                    //this.socketService.emit(prenom +" "+nom+" has quit")
                    window.location.reload();
                }
            },
            (error) => { /*console.log(error);*/ }
        )
    }

    // Request API for updating bio & or avatar
    updateProfile(profile): any{
        return this.http.post<User>( this.updateUrl, profile);
    }

    // Request API for getting user infos by id
    getUserById(id): any{
        return this.http.get<User>( this.userUrl+ "/" +id.toString());
    }

    // get stat data from my personnal stat data system by user id
    getPlayerStat(id): any{
        return this.http.get<PlayerStat>(this.userUrl+"/"+id.toString()+"/playerstat")
    }

    // get history data from my personnal history data system by user id
    getPlayerHistory(id,startIndex,nb): any{
        return this.http.get<QuizzResult>(this.userUrl+"/"+id.toString()+"/playerhistory/"+startIndex.toString()+"/"+nb.toString());
    }

    // get history data from pg "historique" table by user id
    getPgPlayerHistory(id,startIndex,nb): any{
        return this.http.get<QuizzResult>(this.userUrl+"/"+id.toString()+"/pgplayerhistory/"+startIndex.toString()+"/"+nb.toString());
    }

    // get challenge history by userID from table fredouil.hist_defi
    getPlayerChallengeHistory(id,startIndex,nb): any{
        return this.http.get<any>(this.userUrl+"/"+id.toString()+"/challengehistory/"+startIndex.toString()+"/"+nb.toString());
    }

    // get user list between index and index + n
    getUserLists(startIndex:number, nbOfUser:number ): any{
        var resp = this.http.get<User[]>( this.userUrl+"/list/"+startIndex.toString()+"/"+nbOfUser.toString());
        resp.subscribe( list =>{
            this.dataList = list;
            this.updateUserList(this.dataList);
        });
        return resp; 
    }

    // global service call of getUserLists
    getUserList(startIndex=0, nbOfUser=500){
        if ( this.dataList )
            return this.dataList;
        else
            this.getUserLists(startIndex,nbOfUser).subscribe( list =>{
                this.dataList = list;
                return  list;
            });
    }

    // Get a user by ID from local memory
    getLocalUserById(id){
        for ( let user of this.dataList )
            if ( user.id == id )
                return user;
        return undefined;
    }

    // Parse API response and store last connexion
    userLogic(response):any{

        this.data = {
            id: response.id,
            prenom: response.prenom,
            nom: response.nom,
            avatar: response.avatar,
            date_naissance: response.date_naissance,
            humeur: response.humeur,
            statut_connexion: response.statut_connexion
        };
        this.data.date_naissance = this.data.date_naissance.slice(8,10) +
                                   "/"+ this.data.date_naissance.slice(5,7) +
                                   "/"+ this.data.date_naissance.slice(0,4);
        this.updateUser(this.data);
        //this.socketService.emit(this.data.prenom +" "+this.data.nom+" is Online")
        localStorage.setItem('lastConnexion', new Date().toString() );
        //console.log(localStorage.getItem('lastConnexion'));
        return this.data; // Auth Accomplished
    }
}

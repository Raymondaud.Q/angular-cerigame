/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// Question Object Data Shape 
export interface Question
{
	id: number; 
	question: string;
	propositions: string[];
}
 // id | id_user_gagnant | id_user_perdant |         date_defi      

export interface ChallengeResult
{
	winner : number;
	looser : number;
	date : string;
} 
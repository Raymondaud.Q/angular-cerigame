/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// QuizzResult Object Data Shape
export interface QuestionResult
{
	anecdote: String;
    question: String;
    reponse: String;
    correction: String;
    
}
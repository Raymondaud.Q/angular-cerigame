/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// PlayerStat Object Data Shape 
export interface PlayerStat {
	idPostGre : number;
	nbBonneReponse : number; 
	nbQuizz : number; 
	nbTotalReponse : number; 
	sommeScore : number;
	tempsTotal : number;
}

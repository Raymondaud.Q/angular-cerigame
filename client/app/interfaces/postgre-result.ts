// !id  | id_user | date_jeu 	| niveau_jeu | nb_reponses_corr | temps | score 
// !int |  int    | date louche | int 		 | int				| int 	| int

export interface PgResult
{
	id_user : number
	date_jeu : string;
	niveau_jeu : number;
	nb_reponses_corr: number;
	temps : number;
	score : number;
} 
/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// User Object Data Shape 
export interface User {
	id: number;
	prenom: string;
	nom: string;
	avatar: string;
	date_naissance: string;
	humeur: string
	statut_connexion: boolean;
}

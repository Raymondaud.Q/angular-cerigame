/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// PLayerRank Object Data Shape 
export interface PlayerRank {
	idUser: number;
	score: number;
	nbGame: number;
}

/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// QuizzResponse Object Data Shape


import { Response } from './response'
export interface QuizzResponse
{
	idPostGre: number;
	responses: Response[];
	elapsedTime: number;
	difficulty: number;
	challengeId: String;
}
/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// Quizz Object Data Shape 
import { Question } from './question'
export interface Quizz{
	_id: number;
	theme: string;
	redacteur: string;
	fournisseur : string;
	quizz: Question[];
}
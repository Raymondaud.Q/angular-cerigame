/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// Response Object Data Shape 
export interface Response
{
	theme: string;
	idQuestion: number; 
	response: string;
}
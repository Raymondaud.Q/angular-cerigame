/* 
    Author: RAYMONDAUD Quentin
    You can do what you want with this code 
*/

// QuizzResult Object Data Shape
import { QuestionResult } from './question-result';

export interface QuizzResult
{
	nbQuestion: number;
	nbBonneReponse: number;
	temps: number;
	difficulte: number;
	correction: QuestionResult[];
	idWinner: any;
	srcScore: number;
	score: number;
	date: String;
}
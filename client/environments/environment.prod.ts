export const environment = {
  production: true,
  backendBaseKey: 'http://pedago.univ-avignon.fr:3019',
  backendSocketKey: 'ws://pedago.univ-avignon.fr:3019'
}

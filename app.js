 /* 
 * Author:  RAYMONDAUD Quentin
 * helped by : https://medium.com/@xavidramirez/angular-cli-fullstack-nodejs-express-mongoose-part-1-978ea0e3da4d
 */

 /* ng serve --proxy-config proxy.conf.json => ANGULAR SERV:3020 
  * NODE_ENV=prod node app.js  
  *	NODE_ENV=dev  node app.js = node app.js
  * => NODEJS SERVE:3019 PRODUCTION MODE =prod => request on localhost for postgre
  *				                DEV MODE =dev => request on pedago for postgre
  *                				DEFAULT  =dev
*/

const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const MongoDBStore = require('connect-mongodb-session')(session);

const store = new MongoDBStore({
  uri: 'mongodb://127.0.0.1:27017/db',
  collection: 'mySessions',
  touchAfter: 24 * 3600 // sec :  1 sauvegarde toutes les 24h ormis si données MAJ
});

store.on('error', function(error) {
  console.log(error);
});

const app = express();

// Set the ip-address of your trusted reverse proxy server such as 
// haproxy or Apache mod proxy or nginx configured as proxy or others.
// The proxy server should insert the ip address of the remote client
// through request header 'X-Forwarded-For' as
// 'X-Forwarded-For: some.client.ip.address'
// Insertion of the forward header is an option on most proxy software
// Src: https://stackoverflow.com/questions/23413401/what-does-trust-proxy-actually-do-in-express-js-and-do-i-need-to-use-it
app.set('trust proxy', '1');

app.use(require('express-session')({
  secret: 'secret',
  cookie: {
    secure: false,
    maxAge:  24 * 3600 * 1000 // millisecondvaleur par défaut  { path: '/', httpOnly: true, secure: false, maxAge: null}
  },
  store: store,
  // Boilerplate options, see:
  // * https://www.npmjs.com/package/express-session#resave
  // * https://www.npmjs.com/package/express-session#saveuninitialized
  resave: false,      // Session créée uniquement à la première sauvegarde de données
  saveUninitialized: false,   // pas de session sauvegardée si pas de modif
  secret:"Il_ne_faut_pas_le_repeter",
  // cookie: { secure: true } // ( HTTPS only ) Active une protection anti spoofing 
}));

// body parser is quite detailed so read the following article to understand how it works.
// Understanding how Body Parser works : https://medium.com/@adamzerner/how-bodyparser-works-247897a93b90

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// We set the route of the templates and give it to expres
app.set('views', `${__dirname}/server/views`);
// Tell express's engine what file format we are targeting and what
// library to use, in this case we use EJS and it;d renderfile engine
app.engine('html', require('ejs').renderFile);
// And lastly we set the 'view engine' to be HTML
app.set('view engine', 'html');

/**
 * Here we make a variable Routes equal to the routes.js file located in the server folder.
 * In the routes.js file we will make a function that returns all routes for the entire application
 * We then call the function in the Routes variable, giving it the express app variable and __dirname
 */
const Routes = require('./server/routes'); // Import all route endpoints

Routes(app, __dirname);
// Point to static path to dist
// __dirname is retrieved from javascript as the directory name of the location of this file.
// which should be ~/Angular-CLI-Fullstack
app.use(express.static(path.join(__dirname, 'dist')));

// Get port from environment and store in Express
const port = process.env.PORT || '3019';
app.set('port', port);

// CREATE HTTP SERVER
const server = http.createServer(app);

// LISTEN ON PORT
server.listen(port, () => console.log(`API RUNNING ON http://LOCALHOST:${port}/.  mode ${process.env.NODE_ENV || 'dev'}`));

const Socket = require('./server/sockets/socketServer');
serverSocket = Socket(server);



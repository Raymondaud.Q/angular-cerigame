# AngularCERIGame

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.2.

## Development ANGULAR server

Run `ng serve` for a dev server. Navigate to `http://localhost:3020/`. The app will automatically reload if you change any of the source files. ( Only front end )

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.




# Run CERI-Game

Clone and cd in this repo and run `npm i` in order to install project dependencies.

* You need a mongo server hosting a JSON collection with [Many OpenQuizz quizz themes](https://www.openquizzdb.org/download.php) into db.quizz.

* Edit the [env.json](https://gitlab.com/CERI_Raymondaud.Q/angular-cerigame/-/blob/master/server/env/env.json) file according to your network configuration.

* Edit theses [Old PostGre SQL credentials](https://gitlab.com/CERI_Raymondaud.Q/angular-cerigame/-/blob/master/server/components/pgutils/pgUtils.js)

* And maybe edit the [MongoDB setup](https://gitlab.com/CERI_Raymondaud.Q/angular-cerigame/-/blob/master/server/components/mongocli/mongoUtils.js)


##  Front-End

1. Build the Front-End :  

	- If you are on your local machine connected to univ-avignon VPN ( for PgSqlDb ) launch : **npm run start** ( It launches the angular server )
	- If you are on your local machine connected to univ-avignon VPN ( for PgSqlDb ) launch : **npm run buildStage** ( [Set your ip in backendSocketKey and leave backendBaseKey empty ! ](https://gitlab.com/CERI_Raymondaud.Q/angular-cerigame/-/blob/master/client/environments/environment.staging.ts) for testing on multiple device connected to your local machine )
	- If you are on pedago itself launch : **npm run buildProd** ( It just compiles client files, we should not use angular server in prod  )


## Back-End
2. Launch the Back-End :  

	- If you are on your local machine connected to univ-avignon VPN launch : **npm run server** 
		- You can access to Ceri-Game on **localhost:3020** for **npm run start**
		- You can access to Ceri-Game on **<YourIp>:3019** for **npm run buildStage**
	- If you are on pedago itself launch : **npm run serverProd** 
		- You can access to Ceri-Game on **pedago.univ-avignon.fr:3019**

## Important
  
You can look at **scripts** array in [/package.json](https://gitlab.com/CERI_Raymondaud.Q/angular-cerigame/-/blob/master/package.json) in order to know what `npm run` function really do.  
  
Here is and extract of [/package.json](https://gitlab.com/CERI_Raymondaud.Q/angular-cerigame/-/blob/master/package.json) :  
	    "start": "ng serve --proxy-config=./proxy.conf.json",  
	    "build": "ng build",  
	    "buildStage": "ng build --configuration=staging",  
	    "buildProd": "ng build --prod",  
	    "server": "node app.js",  
	    "serverProd": "NODE_ENV=prod node app.js"  
